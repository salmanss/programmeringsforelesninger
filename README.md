# Programmeringsforelesninger

Hei! Her ligger kode, notater, slides/PowerPoint og PDFer fra forelesningene. De første heter Uke XY (1) og Uke XY (2), hvor (1) er mandagstimene og (2) er dobbelttimene (tirsdag/onsdag/fredag).

I hver ukemappe ligger det (muligens med noen unntak):

* en PDF med PowerPoint/Menti-presentasjonen
* en PDF med forelesningsnotater. Dette er ca. det som blir dekket i forelesningene.
* en ipynb-fil med forelesningsnotater. Dette kan brukes med Jupyter Notebook, men det kan være litt stress, så det er ofte like greit å bare bruke PDFen.
* En mappe med kodefiler jeg lagde under forelesning.


## Forelesningsplan
Planen for TDT4109 (et av parallellfagene vårt fag er basert på) hadde sånn ca. følgende forelesningsplan i 2021, og vi kommer nok til å basere oss en del på den. Det kan dermed hende vi ikke følger den helt til punkt å prikke, men det blir nok ikke store forskjeller derfra.

34. Fag- og programmingsintroduksjon
35. Variabler, datatyper, bruk av funksjoner
36. Betingelser og logiske uttrykk
37. Løkker (for og while)
38. Funksjoner
39. Mer om funksjoner, moduler
40. Mer om iterables (lister o.l.)
41. Mer om strings
42. Filer og exceptions
43. Dict og set
44. Algoritmer
45. Programmeringscase
46. Repetisjon
47. Oppsummering/spørretime

Et par forskjeller, per 23. august:

* Vi begynte med variabler og datatyper allerede i uke 34.
* Planen er å begynne å introdusere funksjoner litt tidligere.


## Mer detaljert forelesningsplan

|Uke|Innhold|Mer om innholdet|
|---|---|---|
|34|Fag- og programmeringsintroduksjon|* Generell introduksjon til faget <br>* Installasjon av Thonny <br>* Bruk av CLI og editor|
|35|Variabler, datatyper og bruk av funksjoner|* Variabler <br> * Datatyper og konvertering <br> * Bruk av innebygde funksjoner <br> * Introduksjon til funksjoner|
|36|Betingelser og logiske uttrykk|* bool <br> * if, elif og else|
|37|Løkker|* while <br> * for (range og lister) <br> * Lister|
|38|Funksjoner|* Definisjon/konstruksjon av funksjoner <br> * Input-parametere <br> * Returnering av tuples <br> * Namespace|
|39|Moduler (og mer funksjoner)|* Innebyggede <br> * Installasjon av eksterne moduler i Thonny <br> * Kort om pip|
|40|Mer om iterables|* Lister, tuples og strings <br> * Iterering over itererbare objekter (for) <br> * Mer om indeks <br> * Innbyggede funksjoner (sort, reverse) og len|
|41|Mer om strings|* Innebyggede funksjoner <br> * Iterering (?)|
|42|Filer og exceptions|* Metoder å åpne filer <br> * Lese-/skrivemodus <br> * Generell except <br> * Spesifikke except|
|43|Dict og set|* Intro til begge <br> * Dict-funksjoner og indeksering <br> * Sett i matematikken og i kode|
|44|Algoritmer|?|
|45|Programmeringscase|?|
|46|Repetisjon|?|
|47|Oppsummerings-/spørretime|?|
