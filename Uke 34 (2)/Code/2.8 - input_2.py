# Renters rente beregnes på følgene måte:
# K = K0 * (1 + R)**n

# Variabler brukeren kan fylle inn
startsum = input('Startsum: ')
rente = input('Rente: ')
år = input('År: ')

# Konverter til tall
startsum = int(startsum)
rente = float(rente)
år = int(år)

# Beregning av K
sluttsum = startsum * (1 + rente)**år

# Printe resultat
print('Sluttsum:', sluttsum)
